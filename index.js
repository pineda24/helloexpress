
const express = require('express');

const app = express();

app.get('/hello',(request,response) => {
    response.send('HOLA');

});

app.post('/hello',(request,response) => {
    console.log(request.body);
    response.send('HOLA DESDE POST');
});

app.put('/hello',(request,response) => {
    response.send('HOLA DESDE PUT');
});

app.patch('/hello',(request,response) => {
    response.send('HOLA DESDE PATCH');
});

app.delete('/hello',(request,response) => {
    response.send('HOLA DESDE DELETE');
});

app.listen(4000, () => {
    console.log("App up and running");
});
